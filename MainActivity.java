package com.example.chaman_test_socket_client;

import org.json.*;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Scanner;
import java.net.Socket;
import java.io.IOException;


public class MainActivity extends AppCompatActivity {
public TextView log;
public JSONObject packet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        log = (TextView)findViewById(R.id.log);
        ClientSocket connect = new ClientSocket();
        String[] strings = new String[5];
        connect.execute(strings);

    }
    public JSONObject readJsonFromSocket(BufferedReader buffer)
    {
        String letter;
        String jsonMsg = "";
        JSONObject jsonObject = new JSONObject();
        try{
            while (true) {
                letter = String.valueOf((char)(buffer.read()));
                if(letter.equals("#")){
                    break;
                }
                jsonMsg += letter;
            }
            jsonObject = new JSONObject(jsonMsg);
        }
        catch (Exception e){
            log.setText(e.getMessage());
        }
        return jsonObject;
    }
    private class ClientSocket extends AsyncTask<String, String, String> {
        protected String doInBackground(String... strings) {
            try {
                Socket client_socket = new Socket("192.168.4.52",505);
                BufferedReader in = new BufferedReader(new InputStreamReader(client_socket.getInputStream()));
                boolean isServerOn = true;
                while(isServerOn){
                    packet = readJsonFromSocket(in);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                log.setText(packet.getString("ID"));
                            }
                            catch (Exception e){
                                log.setText(e.getMessage());
                            }
                        }
                    });
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return "";
        }
        /*protected void onProgressUpdate(String msg) {
            log.setText(msg);
            super.onProgressUpdate(msg);
        }*/
    }
}
